import Application from '../Application.js'
import Environment from './Environment.js'
import Floor from './Floor.js'
import Gun from './Gun/Gun.js'
import Tower from './Tower.js'

export default class World {
    constructor() {
        this.application = new Application()

        // Setup
        this.resources = this.application.resources
        this.scene = this.application.scene

        // Listener
        this.resources.on('loaded', () => {
            // World's objects
            this.floor = new Floor()
            this.gun = new Gun()
            this.tower = new Tower()
            this.environment = new Environment()
        })
    }
}
