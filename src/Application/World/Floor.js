import * as THREE from 'three'
import CANNON from 'cannon'

import Application from '../Application.js'

export default class Floor {
    constructor() {
        this.application = new Application()

        // Options
        this.scene = this.application.scene
        this.physics = this.application.physics

        this.setGeometry()
        this.setMaterial()
        this.setMesh()
    }

    setGeometry() {
        this.geometry = new THREE.PlaneGeometry(10, 10)
        // Physics property
        this.shape = new CANNON.Plane()
    }

    setMaterial() {
        this.material = new THREE.MeshStandardMaterial({ color: 0xffffff })
        // Physics property
        this.physicsMaterial = this.physics.defaultMaterial
    }

    setMesh() {
        this.mesh = new THREE.Mesh(this.geometry, this.material)
        this.mesh.receiveShadow = true
        this.mesh.rotation.x = -Math.PI / 2
        this.scene.add(this.mesh)
        // Physics property
        this.body = new CANNON.Body({
            mass: 0,
            shape: this.shape,
            material: this.physicsMaterial,
        })
        this.body.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2)
        this.physics.world.addBody(this.body)
    }
}
