import * as THREE from 'three'

import Application from '../Application'

export default class Environment {
    constructor() {
        this.application = new Application()

        // Setup
        this.scene = this.application.scene

        this.setBaseLight()
        this.setSunLight()
    }

    setBaseLight() {
        this.baseLight = new THREE.AmbientLight('#ffffff', 1)
        this.scene.add(this.baseLight)
    }

    setSunLight() {
        this.sunLight = new THREE.DirectionalLight('#FFDD40', 10)
        this.sunLight.castShadow = true
        this.sunLight.shadow.camera.far = 15
        this.sunLight.shadow.mapSize.set(1024, 1024)
        this.sunLight.position.set(-2, 5, 5)
        this.scene.add(this.sunLight)
    }
}
