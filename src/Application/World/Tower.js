import * as THREE from 'three'
import CANNON from 'cannon'

import Application from '../Application.js'

export default class Tower {
    size = 0.5

    constructor() {
        this.application = new Application()

        // Setup
        this.scene = this.application.scene
        this.physics = this.application.physics
        this.soundpad = this.application.soundpad

        this.setGeometry()
        this.setMaterial()
        this.build()
    }

    setGeometry() {
        this.geometry = new THREE.BoxGeometry(this.size * 3, this.size, this.size)
        this.shape = new CANNON.Box(new CANNON.Vec3((this.size * 3) / 2, this.size / 2, this.size / 2))
    }

    setMaterial() {
        this.material = new THREE.MeshStandardMaterial({
            color: 0xff0000,
            roughness: 0.2,
            metalness: 0.8,
        })
    }

    build() {
        const height = 4
        const width = 3

        for (let i = 0; i < width; i++) {
            for (let j = 0; j < height; j++) {
                this.mesh = new THREE.Mesh(this.geometry, this.material)
                this.mesh.castShadow = true
                this.mesh.position.set(0, j + this.size, i / 2)
                this.scene.add(this.mesh)

                this.body = new CANNON.Body({
                    mass: 1,
                    shape: this.shape,
                    position: this.mesh.position,
                })
                this.physics.world.addBody(this.body)

                this.physics.objectsShouldBeUpdated.push({
                    body: this.body,
                    mesh: this.mesh,
                })

                this.body.addEventListener('collide', (collision) =>
                    this.soundpad.playSound('hit', collision.contact.getImpactVelocityAlongNormal())
                )
            }
        }
    }
}
