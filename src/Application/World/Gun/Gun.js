import Application from '../../Application.js'
import Bullet from './Bullet.js'

export default class Gun {
    MAGAZINE_MAX = 5
    RELOAD_KEY = 82 // R

    constructor() {
        this.application = new Application()

        // Setup
        this.soundpad = this.application.soundpad
        this.ui = this.application.ui
        this.camera = this.application.camera
        this.magazine = new Array(this.MAGAZINE_MAX).fill(new Bullet())

        // Listeners
        window.addEventListener('click', () => {
            if (this.camera.controls.isLocked) {
                this.fire()
            }
        })
        window.addEventListener('keydown', (event) => {
            if (this.camera.controls.isLocked) {
                if (event.keyCode === this.RELOAD_KEY) {
                    this.reload()
                }
            }
        })
    }

    fire() {
        if (this.magazine.length) {
            this.magazine[0].fire()
            this.soundpad.playSound('fire')
            this.magazine.shift()
        } else {
            this.soundpad.playSound('dryFire')
            this.ui.showReloadPopup()
        }
    }

    reload() {
        if (this.magazine.length === this.MAGAZINE_MAX) {
            return this.ui.hideReloadPopup()
        } else {
            this.magazine.push(new Bullet())
            this.soundpad.playSound('reload')
            return this.reload()
        }
    }
}
