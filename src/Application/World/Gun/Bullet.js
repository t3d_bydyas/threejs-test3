import * as THREE from 'three'
import CANNON from 'cannon'

import Application from '../../Application'

export default class Bullet {
    constructor() {
        this.application = new Application()

        // Setup
        this.scene = this.application.scene
        this.resources = this.application.resources
        this.camera = this.application.camera
        this.physics = this.application.physics
    }

    #getFireDirection() {
        const vector = new THREE.Vector3(0, 0, 1)
        vector.unproject(this.camera.instance)
        const ray = new THREE.Ray(this.body.position, vector.sub(this.body.position).normalize())
        return ray.direction
    }

    setModel() {
        this.model = this.resources.items.hamburgerModel.scene.clone()
        this.model.scale.set(0.05, 0.05, 0.05)
        this.model.position.copy(this.camera.instance.position)

        this.model.traverse((child) => {
            if (child instanceof THREE.Mesh) {
                child.castShadow = true
            }
        })

        this.scene.add(this.model)
    }

    setPhysics() {
        this.body = new CANNON.Body({
            mass: 1,
            shape: new CANNON.Sphere(0.15),
            position: this.camera.instance.position,
        })

        const force = 30
        const fireDirection = this.#getFireDirection()
        this.body.velocity.set(fireDirection.x * force, fireDirection.y * force, fireDirection.z * force)

        this.physics.world.addBody(this.body)
    }

    fire() {
        this.setModel()
        this.setPhysics()

        this.physics.objectsShouldBeUpdated.push({
            body: this.body,
            mesh: this.model,
        })
    }
}
