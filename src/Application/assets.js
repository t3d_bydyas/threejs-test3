export default [
    {
        name: 'hamburgerModel',
        type: 'model',
        path: '/models/hamburger.glb',
    },
    {
        name: 'reloadSound',
        type: 'sound',
        path: '/sounds/reload.mp3',
    },
    {
        name: 'fireSound',
        type: 'sound',
        path: '/sounds/fire.mp3',
    },
    {
        name: 'dryFireSound',
        type: 'sound',
        path: '/sounds/dryFire.mp3',
    },
    {
        name: 'hitSound',
        type: 'sound',
        path: '/sounds/hit.mp3',
    },
    {
        name: 'introSound',
        type: 'sound',
        path: '/sounds/intro.mp3',
    },
]
