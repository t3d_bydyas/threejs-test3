import * as THREE from 'three'
import { PointerLockControls } from 'three/addons/controls/PointerLockControls.js'

import Application from './Application.js'

export default class Camera {
    fov = 75
    near = 0.01
    far = 20

    constructor() {
        this.application = new Application()

        // Setup
        this.sizes = this.application.sizes
        this.scene = this.application.scene
        this.canvas = this.application.canvas

        this.setInstance()
        this.setPointerLockControls()
    }

    setInstance() {
        this.instance = new THREE.PerspectiveCamera(this.fov, this.sizes.width / this.sizes.height, this.near, this.far)
        this.instance.position.set(-3, 1, 2)
        this.scene.add(this.instance)
    }

    setPointerLockControls() {
        this.controls = new PointerLockControls(this.instance, this.canvas)
    }

    resize() {
        this.instance.aspect = this.sizes.width / this.sizes.height
        this.instance.updateProjectionMatrix()
    }
}
