import CANNON from 'cannon'
import Application from './Application'

export default class Physics {
    EARTH_GRAVITY = -9.807

    constructor() {
        this.application = new Application()

        // Options
        this.time = this.application.time
        this.world = new CANNON.World()
        this.objectsShouldBeUpdated = []

        this.setConfiguration()
        this.setDefaultContactMaterial()
    }

    setConfiguration() {
        this.world.broadphase = new CANNON.SAPBroadphase(this.world)
        this.world.allowSleep = true
        this.world.gravity.set(0, this.EARTH_GRAVITY, 0)
    }

    setDefaultContactMaterial() {
        this.defaultMaterial = new CANNON.Material('default')
        this.defaultContactMaterial = new CANNON.ContactMaterial(this.defaultMaterial, this.defaultMaterial, {
            friction: 1,
            restitution: 0.1,
            contactEquationStiffness: 5e6,
            contactEquationRelaxation: 3,
        })
        this.world.addContactMaterial(this.defaultContactMaterial)
        this.world.defaultContactMaterial = this.defaultContactMaterial
    }

    update() {
        this.world.step(1 / 60, this.time.delta, 3)

        if (this.objectsShouldBeUpdated.length) {
            for (const object of this.objectsShouldBeUpdated) {
                object.mesh.position.copy(object.body.position)
                object.mesh.quaternion.copy(object.body.quaternion)
            }
        }
    }
}
