import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js'

import EventEmitter from './EventEmitter.js'

export default class Resources extends EventEmitter {
    constructor(assets) {
        super()

        // Options
        this.assets = assets

        // Setup
        this.items = {}
        this.toLoad = this.assets.length
        this.loaded = 0

        this.setLoaders()
        this.startLoading()
    }

    setLoaders() {
        this.loaders = {}
        this.loaders.gltfLoader = new GLTFLoader()
    }

    startLoading() {
        for (const asset of this.assets) {
            switch (asset.type) {
                case 'model':
                    this.loaders.gltfLoader.load(asset.path, (file) => this.assetLoaded(asset, file))
                    break
                case 'sound':
                    this.assetLoaded(asset, asset.path)
            }
        }
    }

    assetLoaded(asset, file) {
        this.items[asset.name] = file

        this.loaded++

        if (this.loaded === this.toLoad) {
            this.trigger('loaded')
        }
    }
}
