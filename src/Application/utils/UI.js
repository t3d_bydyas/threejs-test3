import Application from '../Application'

export default class UI {
    constructor() {
        this.application = new Application()

        // Setup
        this.camera = this.application.camera
        this.soundpad = this.application.soundpad
        this.reloadPopup = document.querySelector('.alert')
        this.menu = document.querySelector('.menu')
        this.playIntro = true

        // Listeners
        this.menu.addEventListener('click', () => {
            this.camera.controls.lock()
            if (this.playIntro) {
                this.soundpad.playSound('intro')
            }
            this.playIntro = false
        })
        this.camera.controls.addEventListener('lock', () => (this.menu.style.display = 'none'))
        this.camera.controls.addEventListener('unlock', () => (this.menu.style.display = 'flex'))
    }

    showReloadPopup() {
        this.reloadPopup.classList.add('visible')
    }

    hideReloadPopup() {
        this.reloadPopup.classList.remove('visible')
    }
}
