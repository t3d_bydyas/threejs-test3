import Application from '../Application.js'

export default class Soundpad {
    constructor() {
        this.application = new Application()

        // Setup
        this.resources = this.application.resources

        this.setSounds()
    }

    setSounds() {
        this.sounds = {}
        for (const sound in this.resources.items) {
            this.sounds[sound.slice(0, -5)] = new Audio(this.resources.items[sound])
        }
    }

    playSound(sound, impactStrength = 2) {
        if (impactStrength > 1.5) {
            this.sounds[sound].currentTime = 0
            this.sounds[sound].play()
        }
    }
}
